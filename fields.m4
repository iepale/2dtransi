divert(-1)
define(`gitex_title', One and two dimensional diffusion kinetics benchmarks         `')
define(`gitex_subtitle',   Milonga capabilities to solve time dependent neutron diffusion problems     `')  # optional

#define(`gitex_docnumber',        `SP-GITEX-16-2EA8')
# a nice and nerdy way to number documents is to compute the first four digits of the MD5 sum of the title
define(`gitex_docnumber',        `SP-GITEX-16-esyscmd(echo gitex_title | md5sum | cut -c-4 | tr -d "\n" | tr "[:lower:]" "[:upper:]")')

define(`gitex_abstract',`
One and two dimension time dependent benchmarks were solved with a milonga branch.
These benchmarks are useful to test the feasibility of adding this feature to milonga.
Only data and main results are reported because milonga is open source, so all the information is available on the internet.
All the results matched the references.'
)


#define(`gitex_doctype',          `Description')
#define(`gitex_mainlanguage',     `english')
#define(`gitex_otherlanguages',   `spanish')
#define(`gitex_fontsize',         `11pt')

#define(`gitex_institution',      `Seamplex')
#define(`gitex_keywords',         `Git, latex')

## if this macro is not defined, the document author is taken as the author of the commit marked as the repository head
define(`gitex_firstauthor',     `Theler, G.G.')
define(`gitex_firstauthoremail',`jeremy@seamplex.com')

define(`gitex_secondauthor',     `Camusso, C.P.')
define(`gitex_secondauthoremail',`cepasarblo@yahoo.com.ar')

#define(`gitex_reviewer',         `My boss')
#define(`gitex_revieweremail',    `boss@internet.com')

#define(`gitex_releaser',         `His boss')
#define(`gitex_releaseremail',    `chief@internet.com')

#define(`gitex_client',           `Pinky \& Brain')
#define(`gitex_project',          `Conquer the World')

define(`gitex_includerev',        `')
define(`gitex_includetoc',        `')
#define(`gitex_includelof',        `')

define(`gitex_logo1hor',          `logo1-hor.pdf')

define(`gitex_logo1ver',          `logo1-ver.pdf')
define(`gitex_logo1ver_width',    `25')   # in mm

#define(`gitex_logo2ver',          `logo2-ver.png')
#define(`gitex_logo2ver_width',    `50')   # in mm

#define(`gitex_distribution', `
#Dr. Sheldon Cooper     & CalTech      & scooper@caltech.edu\\
#Dr. Leonard Hofstaeder & CalTech      & lhofstaeder@caltech.edu\\
#Mr. Howard Wolowitz    & CalTech      & hwolowitz@caltech.edu\\
#')
#define(`gitex_cc', `
#Dr. Amy Farah Fowler   &                        & \\
#Mrs. Penny             & Cheese Cake Factory    & \\
#')

define(`gitex_extrapreamble',         
`\newcommand{\wasmail}{wasora@samplex.com} 
\newcommand{\wasbit}{https://bitbucket.org/wasora/wasora} 
\newcommand{\milbit}{https://bitbucket.org/wasora/milonga}
\usepackage[labelfont=bf]{caption}'
)

include(`defaults.m4')
divert(0)dnl
