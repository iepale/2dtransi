% body contents
%  pandoc README.md --no-wrap -t latex | sed s/subsection/section/ | sed s/dnl/\`dnl\'/

\section{Introduction}

Milonga \cite{WA-MI-AR-14-11D3} is a is a free core-level neutronic code that solves the steady-state multigroup neutron transport equation (either using the diffusion approximation or the discrete ordinates $S_N$ method) over unstructured grids (although simple structured grids can also be used) using either a finite-volumes or a finite-elements discretization scheme.
It works on top of the wasora \cite{SP-WA-15-TD-9E3D} framework, which provides means to parse and understand a high-level plain-text input file containing algebraic expressions, data for function interpolation, differential equations and output instructions amongst other facilities.
Therefore, any mathematical computation which can be done by wasora---i.e. parametric calculations, multidimensional optimization, function interpolation and integration, etc.---can be combined with the facilities that milonga provides to solve the neutron diffusion equation.

Calculating fast transients, in which the flux's time derivative cannot be disregarded, 
is a new feature which is being studied and developed to be added to milonga and, so far, it is a different branch. A set of benchmarks were solved:

%  An invented benchmark.
  ARGONNE CODE CENTER: BENCHMARK PROBLEM BOOK. Identification 6: Infinite Slab Reactor Model \cite{refB6A1}.

  ARGONNE CODE CENTER: BENCHMARK PROBLEM BOOK. Identification 14-A1:  Super Prompt-Critical Transient; Two-dimensional 
Neutron Diffusion Problem, with Adiabatic Heatup and Doppler Feedback in Thermal Reactor \cite{refB14A1, twigl}.

  Twigl \cite{twigl}.

In order to solve transients, the delayed neutrons precursors were solved together with the flux in the same equation system.
It means they were not added as en external source and there is no need to calculate coupling coefficients.

It is implemented in the finite elements scheme and it is a future work to implement it in finite volumes scheme.

The initial condition is calculated from the critical condition, which is got by dividing the fission cross sections by $k_{eff}$,
and the initial precursor concentrations are in equilibrium with the initial critical flux distribution.

\section{Infinite Slab Reactor Model}

This benchmark is a set of four ones. Their difference is in the perturbation.

The geometry is one dimensional (\autoref{fig:geo1}). The reactor consists of three zones with the data shown in \autoref{tab:data1}, the two ones which have a zero boundary condition
have the same data at time equal to zero.

The delayed neutron data data is shown in the \autoref{tab:DN1}.

There are not data about the energy per fission, so it is considered equal to the fission cross section.

There are not data about the delayed neutron emission spectrum, so they are assumed to be equal to the fission spectrum.

Second order elements are used.

The initial $k_{eff}$ is the same for all the cases: $k_{eff}=0.9015186$ while the reference $k_{eff}$ is $k_{eff}=0.9015507$

The initial power fraction normalized to 1 is shown in the \autoref{tab:B6A1inifrac} and it is the same for all the cases.

The initial power fraction is not symmetrical (\autoref{tab:B6A1inifrac}) because the solver gives a non symmetrical solution (\autoref{fig:B6A1power}); but it is a small difference.

\begin{figure}[h]
%\captionsetup{type=figure}
\centering
\includegraphics[width=8cm]{geom}
\caption{Geometry configuration}\label{fig:geo1}
\end{figure}

\vspace{1cm}

\noindent
\begin{minipage}{7cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
                    & \multicolumn{2}{|c|}{\textbf{Region}}     \\ \cline{2-3}
\textbf{Constant}                           & 1 and 3   & 2     \\ \hline 
D\textsuperscript{1} [cm]                   & 1.5       & 1     \\ \hline
D\textsuperscript{2} [cm]                   & 0.5       & 0.5   \\ \hline
$\Sigma_a^1$ [cm\textsuperscript{-1}]\textsuperscript{a}       & 0.026     & 0.02  \\ \hline
$\Sigma_a^2$ [cm\textsuperscript{-1}]\textsuperscript{a}       & 0.18      & 0.08  \\ \hline
$\nu\Sigma_f^1$ [cm\textsuperscript{-1}]    & 0.01      & 0.005 \\ \hline
$\nu\Sigma_f^2$ [cm\textsuperscript{-1}]    & 0.2       & 0.099 \\ \hline
$\Sigma^{1\to2}$ [cm\textsuperscript{-1}]   & 0.015     & 0.01  \\ \hline
$\chi^{1}$ [-]                              &  1        & 1     \\ \hline
$\chi^{2}$ [-]                              & 0         & 0     \\ \hline
v\textsuperscript{1} [cm/s]                 & 10\textsuperscript{7}     & 10\textsuperscript{7}  \\ \hline
v\textsuperscript{2} [cm/s]                 & 3 10\textsuperscript{5}   & 3 10\textsuperscript{5}\\ \hline
\end{tabular}

\small
\textsuperscript{a} Total removal cross section, including $\Sigma_C$, $\Sigma_f$, and $\Sigma^{1\to2}$.
\captionof{table}{Initial two groups constants}\label{tab:data1}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
                    & \textbf{Effective}        & \textbf{Decay}    \\ 
\textbf{Type}       & \textbf{Delay Fraction}   & \textbf{Constant [s\textsuperscript{-1}]}      \\ \hline 
1      & 0.00025   & 0.0124   \\ \hline
2      & 0.00164   & 0.0305   \\ \hline
3      & 0.00147   & 0.111    \\ \hline
4      & 0.00296   & 0.301    \\ \hline
5      & 0.00086   & 1.14     \\ \hline
6      & 0.00032   & 3.01     \\ \hline
\end{tabular}
\captionof{table}{Delayed neutron parameters}\label{tab:DN1}
\end{minipage}

\begin{table}[h]
%\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Region}   & \multicolumn{2}{|c|}{\textbf{Power [-]}} \\ \cline{2-3}
                  & Milonga & Reference \\ \hline 
1        & 0.27881 & 0.27895 \\ \hline
2        & 0.44242 & 0.44209 \\ \hline
3        & 0.27884 & 0.27895 \\ \hline
\end{tabular}
\caption{Initial power fractions}\label{tab:B6A1inifrac}
\end{table}

\subsection{Subcritical Transient, 1D 2-groups Neutron Diffusion Problem in Thermal Reactor}

The initiating perturbation is that $\Sigma_a^2$ in region 1 is linearly increased by 3\% in 1 second.

The mesh is uniform with $\Delta x=$2cm to compare with \cite{refB6A1}.

The time step is 0.1 seconds and it is solved with the backwards Euler method.

Results:

  The thermal flux at 0, 1 and 2 second is shown in the \autoref{fig:B6A1thermal}.

  The fast flux at 0, 1 and 2 second is shown in the \autoref{fig:B6A1fast}.

  The power at 0, 1 and 2 second is shown in the \autoref{fig:B6A1power}.

  The total power relative to the initial total power is shown in the \autoref{tab:B6A1pow}.

  The power fractions relative to the initial power fractions is shown in the \autoref{tab:B6A1fracpow}.

%\vfill
\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A1ter}
\captionof{figure}{Thermal flux}\label{fig:B6A1thermal}
\end{minipage}
%\vspace{1cm}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A1fast}
\captionof{figure}{Fast flux}\label{fig:B6A1fast}
\end{minipage}

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A1pow}
\captionof{figure}{Power}\label{fig:B6A1power}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Time [s]} & \multicolumn{2}{|c|}{\textbf{Power [-]}} \\ \cline{2-3} 
                  & Milonga & Reference  \\ \hline 
0        &   1      &  1        \\ \hline
0.1      &  0.9290  &  0.9298   \\ \hline
0.2      &  0.8720  &  0.8732   \\ \hline
0.5      &  0.7582  &  0.7596   \\ \hline
1        &  0.6577  &  0.6588   \\ \hline
1.5      &  0.6425  &  0.6432   \\ \hline
2        &  0.6301  &  0.6306   \\ \hline
\end{tabular}
\captionof{table}{Total Power}\label{tab:B6A1pow}
\end{minipage}

\noindent
\begin{center}
\begin{minipage}{12cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}  \hline 
         & \multicolumn{6}{|c|}{\textbf{Region}} \\ \cline{2-7}
         & \multicolumn{3}{|c|}{Milonga} & \multicolumn{3}{|c|}{Reference} \\ \cline{2-7}
\textbf{Time [s]} & 1 & 2 & 3  & 1 & 2 & 3 \\ \hline 
0        &   1     & 1      & 1       &   1   &  1   &  1      \\ \hline
0.1      & 0.8607  & 0.9331 & 0.9907  &0.8621 &0.9339&0.9910   \\ \hline
0.2      & 0.7499  & 0.8792 & 0.9826  &0.7520 &0.8804&0.9830   \\ \hline
0.5      & 0.5313  & 0.7709 & 0.9649  &0.5336 &0.7724&0.9655   \\ \hline
1        & 0.3437  & 0.6742 & 0.9456  &0.3452 &0.6753&0.9462   \\ \hline
1.5      & 0.3226  & 0.6581 & 0.9377  &0.3235 &0.6587&0.9381   \\ \hline
2        & 0.3058  & 0.6449 & 0.9307  &0.3066 &0.6455&0.9311   \\ \hline
\end{tabular}
\captionof{table}{Power fractions}\label{tab:B6A1fracpow}
\end{minipage}
\end{center}

\subsection{Delayed Super-critical Transient, 1D 2-groups Neutron Diffusion Problem in Thermal Reactor}
The initiating perturbation is that $\Sigma_a^2$ in region 1 is linearly decreased by 1\% in 1 second.

The mesh is uniform with $\Delta x=$2cm to compare with \cite{refB6A1}.

The time step is 0.05 seconds and it is solved with the Crank-Nicolson method.

Results:

  The thermal flux at 0, 2 and 4 second is shown in the \autoref{fig:B6A2thermal}.

  The fast flux at 0, 2 and 4 second is shown in the \autoref{fig:B6A2fast}.

  The power at 0, 2 and 4 second is shown in the \autoref{fig:B6A2power}.

  The total power relative to the initial total power is shown in the \autoref{tab:B6A2pow}.

  The power fractions relative to the initial power fractions is shown in the \autoref{tab:B6A2fracpow}.

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A2ter}
\captionof{figure}{Thermal flux}\label{fig:B6A2thermal}
\end{minipage}
%\vspace{1cm}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A2fast}
\captionof{figure}{Fast flux}\label{fig:B6A2fast}
\end{minipage}

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A2pow}
\captionof{figure}{Power}\label{fig:B6A2power}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Time [s]} & \multicolumn{2}{|c|}{\textbf{Power [-]}} \\ \cline{2-3} 
                  & Milonga & Reference  \\ \hline 
0        &   1      &   1        \\ \hline
0.1      &  1.0326  &  1.028     \\ \hline
0.2      &  1.0695  &  1.062     \\ \hline
0.5      &  1.2194  &  1.205     \\ \hline
1        &  1.7864  &  1.740     \\ \hline
1.5      &  1.9701  &  1.959     \\ \hline
2        &  2.1771  &  2.165     \\ \hline
3        &  2.6192  &  2.605     \\ \hline
4        &  3.1246  &  3.107     \\ \hline
\end{tabular}
\captionof{table}{Total Power}\label{tab:B6A2pow}
\end{minipage}

\noindent
\begin{center}
\begin{minipage}{12cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}  \hline 
         & \multicolumn{6}{|c|}{\textbf{Region}} \\ \cline{2-7}
         & \multicolumn{3}{|c|}{Milonga} & \multicolumn{3}{|c|}{Reference} \\ \cline{2-7}
\textbf{Time [s]} & 1 & 2 & 3  & 1 & 2 & 3 \\ \hline 
0        &   1     & 1      & 1      &    1   &   1    &    1    \\ \hline
0.1      & 1.0634  & 1.0310 & 1.0046 & 1.056  & 1.027  &  1.004  \\ \hline
0.2      & 1.1350  & 1.0658 & 1.0096 &   -    &  -     &    -    \\ \hline
0.5      & 1.4264  & 1.2078 & 1.0308 & 1.399  & 1.193  &  1.028  \\ \hline
1        & 2.5248  & 1.7439 & 1.1138 & 2.435  & 1.701  &  1.107  \\ \hline
1.5      & 2.8600  & 1.9216 & 1.1553 &  -     &   -    &   -     \\ \hline
2        & 3.2379  & 2.1236 & 1.2012 & 3.215  & 2.113  &  1.119  \\ \hline
3        & 4.0426  & 2.5524 & 1.3019 & 4.016  & 2.539  &  1.298  \\ \hline
4        & 4.9589  & 3.0426 & 1.4205 & 4.927  & 3.026  &  1.416  \\ \hline
\end{tabular}
\captionof{table}{Power fractions}\label{tab:B6A2fracpow}
\end{minipage}
\end{center}
\subsection{Prompt Super-critical Transient, 1D 2-groups Neutron Diffusion Problem in Thermal Reactor}
The initiating perturbation is that $\Sigma_a^2$ in region 1 is linearly decreased by 5\% in 0.01 second.

The mesh is uniform with $\Delta x=$2cm to compare with \cite{refB6A1}.

The time step is 10\textsuperscript{-5} seconds and it is solved with the Crank-Nicolson method.

Results:

  The thermal flux at 0, 0.01 and 0.02 second is shown in the \autoref{fig:B6A3thermal}.

  The fast flux at 0, 0.01 and 0.02 second is shown in the \autoref{fig:B6A3fast}.

  The power at 0, 0.01 and 0.02 second is shown in the \autoref{fig:B6A3power}.

  The total power relative to the initial total power is shown in the \autoref{tab:B6A3pow}.

  The power fractions relative to the initial power fractions is shown in the \autoref{tab:B6A3fracpow}.

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A3ter}
\captionof{figure}{Thermal flux}\label{fig:B6A3thermal}
\end{minipage}
%\vspace{1cm}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A3fast}
\captionof{figure}{Fast flux}\label{fig:B6A3fast}
\end{minipage}

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A3pow}
\captionof{figure}{Power}\label{fig:B6A3power}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Time [s]} & \multicolumn{2}{|c|}{\textbf{Power [-]}} \\ \cline{2-3} 
                  & Milonga & Reference  \\ \hline 
0        &   1      &    1     \\ \hline
0.001    &  1.0224  & 1.022    \\ \hline
0.005    &  1.6594  & 1.659    \\ \hline
0.01     &  15.62   & 15.65    \\ \hline
0.012    &  69.90   & 70.19    \\ \hline
0.015    &  674.8   & 680.3    \\ \hline
0.018    & 6531     & 6611     \\ \hline
0.02     & 29669    & 30110    \\ \hline
\end{tabular}
\captionof{table}{Total Power}\label{tab:B6A3pow}
\end{minipage}

\noindent
\begin{center}
\begin{minipage}{12cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}  \hline 
         & \multicolumn{6}{|c|}{\textbf{Region}} \\ \cline{2-7}
         & \multicolumn{3}{|c|}{Milonga} & \multicolumn{3}{|c|}{Reference} \\ \cline{2-7}
\textbf{Time [s]} & 1 & 2 & 3  & 1 & 2 & 3 \\ \hline 
0        &   1     & 1      & 1       &  1     &  1    &   1      \\ \hline
0.001    & 1.058   & 1.014  & 1       & 1.058  & 1.014 &  1       \\ \hline
0.005    & 2.485   & 1.543  & 1.017   & 2.484  & 1.544 &  1.017   \\ \hline
0.01     & 34.77   & 12.56  & 1.341   & 34.81  & 12.58 &  1.342   \\ \hline
0.012    & 160.3   & 55.45  & 2.396   &  -     &   -   &    -     \\ \hline
0.015    & 1558    & 534.4  & 14.75   & 1570   & 538.8 &  14.85   \\ \hline
0.018    & 15086   & 5172   & 134.8   &   -    &   -   &    -     \\ \hline
0.02     & 68532   & 23496  & 609.1   & 69540  & 23850 &  617.9   \\ \hline
\end{tabular}
\captionof{table}{Power fractions}\label{tab:B6A3fracpow}
\end{minipage}
\end{center}
\subsection{Prompt Super-critical Transient, 1D 2-groups Neutron Diffusion Problem in Thermal Reactor with Modified Neutron Velocities}
The initiating perturbation is that $\Sigma_a^2$ in region 1 is linearly decreased by 5\% in 0.01 second.

The mesh is uniform with $\Delta x=$2cm to compare with \cite{refB6A1}.

The time step is 10\textsuperscript{-6} seconds and it is solved with the Crank-Nicolson method.

The fast group velocity is 10\textsuperscript{9} cm/s and the thermal velocity is 3 10\textsuperscript{7} cm/s.

Results:

  The thermal flux at 0, 0.003 and 0.005 second is shown in the \autoref{fig:B6A4thermal}.

  The fast flux at 0, 0.003 and 0.005 second is shown in the \autoref{fig:B6A4fast}.

  The power at 0, 0.003 and 0.005 second is shown in the \autoref{fig:B6A4power}.

  The total power relative to the initial total power is shown in the \autoref{tab:B6A4pow}.

  The power fractions relative to the initial power fractions is shown in the \autoref{tab:B6A4fracpow}.

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A4ter}
\captionof{figure}{Thermal flux}\label{fig:B6A4thermal}
\end{minipage}
%\vspace{1cm}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A4fast}
\captionof{figure}{Fast flux}\label{fig:B6A4fast}
\end{minipage}

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\includegraphics[width=8cm]{B6A4pow}
\captionof{figure}{Power}\label{fig:B6A4power}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Time [s]} & \multicolumn{2}{|c|}{\textbf{Power [-]}} \\ \cline{2-3} 
                  & Milonga & Reference  \\ \hline 
0        &   1      &   1      \\ \hline
0.001    & 1.178    & 1.178    \\ \hline
0.002    & 1.558    & 1.558    \\ \hline
0.003    & 2.796    & 2.797    \\ \hline
0.0035   & 5.279    & 5.284    \\ \hline
0.004    & 20.66    & 20.72    \\ \hline
0.0045   & 467.2    & 472.0    \\ \hline
0.005    & 150268   & 153700   \\ \hline
\end{tabular}
\captionof{table}{Total Power}\label{tab:B6A4pow}
\end{minipage}

\noindent
\begin{center}
\begin{minipage}{12cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}  \hline 
         & \multicolumn{6}{|c|}{\textbf{Region}} \\ \cline{2-7}
         & \multicolumn{3}{|c|}{Milonga} & \multicolumn{3}{|c|}{Reference} \\ \cline{2-7}
\textbf{Time [s]} & 1 & 2 & 3  & 1 & 2 & 3 \\ \hline 
0        &   1     & 1      & 1      &  1    &   1   &   1    \\ \hline
0.001    & 1.351   & 1.167  & 1.022  & 1.351 & 1.167 & 1.022  \\ \hline
0.002    & 2.101   & 1.524  & 1.070  & 2.101 & 1.525 & 1.070  \\ \hline
0.003    & 4.546   & 2.685  & 1.222  & 4.547 & 2.686 & 1.222  \\ \hline
0.0035   & 9.473   & 5.007  & 1.516  &   -   &  -    &   -    \\ \hline
0.004    & 40.24   & 19.31  & 3.216  & 40.36 & 19.37 & 3.224  \\ \hline
0.0045   & 946.7   & 430.3  & 46.65  &   -   &  -    &  -     \\ \hline
0.005    & 310126  & 136409 & 12427  & 317100&139500 & 12710  \\ \hline
\end{tabular}
\captionof{table}{Power fractions}\label{tab:B6A4fracpow}
\end{minipage}
\end{center}
\subsection{Conclusion}
The comparisons with \cite{refB6A1} show that milonga agrees with the reference.
\section{Two dimensional BWR transient}
This benchmark is the 2D case of the LRA BWR Kinetics Problem.

The identification of this benchmark is Super Prompt-Critical Transient; Two-dimensional 
Neutron Diffusion Problem, with Adiabatic Heatup and Doppler Feedback in Thermal Reactor \cite{refB14A1, twigl}.

This benchmark simulates four control rod ejection because it happens in a quarter of core.

It is a two-dimensional (xy), two groups diffusion theory.

Two delayed neutron precursor groups with zero flux boundary conditions on external surfaces,
reflection conditions at symmetry boundaries, and steady state initial conditions, all the fission neutrons appear in the fast flux.

The equations to be solved are:

\begin{align*}
\nabla D_1(\mathbf{x}, t)\nabla \Phi_1(\mathbf{x}, t)-\left[\Sigma a_1(\mathbf{x}, t)+\Sigma_{1\to2}(\mathbf{x}, t)\right]\Phi_1(\mathbf{x}, t)+ \\
\nu(1-\beta)\left[\Sigma f_1(\mathbf{x}, t)\Phi_1(\mathbf{x}, t)+\Sigma f_2\Phi_2(\mathbf{x}, t)\right]+\sum_{i=1}^{2}\lambda_i C_i(\mathbf{x},t)=\frac{1}{v_1}\frac{\partial\Phi_1(\mathbf{x}, t)}{\partial t} \\
\nabla D_2(\mathbf{x}, t)\nabla \Phi_2(\mathbf{x}, t)-\Sigma a_2(\mathbf{x}, t)\Phi_2(\mathbf{x}, t)+\Sigma_{1\to2}(\mathbf{x}, t)\Phi_1(\mathbf{x}, t)= 
\frac{1}{v_2}\frac{\partial\Phi_2(\mathbf{x}, t)}{\partial t} \\
\nu \beta_i \left[\Sigma f_1(\mathbf{x}, t)\Phi_1(\mathbf{x}, t)+\Sigma f_2(\mathbf{x}, t)\Phi_2(\mathbf{x}, t)\right]-\lambda_i C_i(\mathbf{x}, t)=\frac{\partial C_i}{\partial t}, ~i=1,2.
\end{align*}

Adiabatic heatup:

\begin{equation*}
\alpha \left[ \Sigma f_1(\mathbf{x}, t)\Phi_1(\mathbf{x}, t)+\Sigma f_2(\mathbf{x}, t)\Phi_2(\mathbf{x}, t)\right]=\frac{\partial T(\mathbf{x}, t)}{\partial t}
\end{equation*}

Doppler feedback:

\begin{equation*}
\Sigma a_1(\mathbf{x}, t)=\Sigma a_1(\mathbf{x}, 0)\left[1+\gamma \left(\sqrt{T(\mathbf{x}, t)}-\sqrt{T_0}\right)\right]
\end{equation*}

Power:

\begin{equation*}
P(\mathbf{x}, t)=\epsilon \left[ \Sigma f_1(\mathbf{x}, t)\Phi_1(\mathbf{x}, t)+\Sigma f_2(\mathbf{x}, t)\Phi_2(\mathbf{x}, t)\right]
\end{equation*}

The \autoref{fig:B14geom2d} shows the geometry used and the \autoref{tab:B14matdata} the material data.

Additional parameters for all Regions:

B\textsuperscript{2}=10\textsuperscript{-4} axial buckling for both energy groups.

$\nu$=2.43 mean number of neutrons per fission.

$v_1$= 3 10\textsuperscript{7} cm s\textsuperscript{-1}

$v_2$= 3 10\textsuperscript{5} cm s\textsuperscript{-1}

The \autoref{tab:B14delayedata} shows the delayed neutron data. Those values are not the ones reported in \cite{refB14A1}, but the ones reported in \cite{twigl}.

Data for Feedback model \cite{twigl}:

$\alpha$=3.83 10\textsuperscript{-11} K cm\textsuperscript{3} conversion factor.

$\gamma$=3.034 10\textsuperscript{-3} K\textsuperscript{0.5} feedback constant.

$\epsilon$=3.204 10\textsuperscript{-11} Ws/fission energy conversion factor.

The initial condition is made critical by dividing the production cross sections by $k_{eff}$. The initial flux distribution 
shall be normalized such that the average power density:

\begin{equation*}
\bar{P}=\frac{\epsilon} {V_{core}}\int_{Vcore} \left[ \Sigma f_1(\mathbf{x}, t)\Phi_1(\mathbf{x}, t)+\Sigma f_2(\mathbf{x}, t)\Phi_2(\mathbf{x}, t)\right] \mathrm{d}V=
1~10^{-6} ~W~cm^{-3}
\end{equation*}

The initial precursor concentrations are in equilibrium with the initial critical flux distribution.

The initial temperature is $T_0=300$ K.

\begin{figure}[ht]
\centering
\includegraphics[width=12cm]{B14geom2d}
\caption{Geometry used to solve the 2-D LRA BWR benchmark}\label{fig:B14geom2d}
\end{figure}
\begin{table}[ht]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}  \hline 
\textbf{Region} & \textbf{Material} & \textbf{Group i} & $\mathbf{\textbf{D}_i} [cm]$ & $\Sigma a_i$ [$cm^{-1}$] &$\nu \Sigma f_i$ [$cm^{-1}]$ &$\Sigma_{1\to2}$ [$cm^{-1}]$ \\ \hline 
1  & Fuel 1 with      & 1 & 1.255  & 0.008252  & 0.004602 & - \\
   & rod              & 2 & 0.211  & 0.1003    & 0.1091   & 0.02533 \\ \hline
2  & Fuel 1 without   & 1 & 1.268  & 0.007181  & 0.004609 & - \\
   & rod              & 2 & 0.1902 & 0.07047   & 0.08675  & 0.02767 \\ \hline
3  & Fuel 2 with      & 1 & 1.259  & 0.008002  & 0.004663 & - \\
   & rod              & 2 & 0.2091 & 0.08344   & 0.1021   & 0.02617 \\ \hline
4  & Fuel 2 without   & 1 & 1.259  & 0.008002  & 0.004663 & - \\
   & rod              & 2 & 0.2091 & 0.073324  & 0.1021   & 0.02617 \\ \hline
5  & Reflector        & 1 & 1.257  & 0.0006034 & 0        & - \\
   &                  & 2 & 0.1592 & 0.01911   & 0        & 0.04754 \\ \hline
\end{tabular}
\caption{Material data for the 2-D LRA BWR benchmark}\label{tab:B14matdata}
\end{table}
\begin{table}[ht]
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Group} & $\mathbf{\beta_i}$ & $\mathbf{\lambda_i}$ [s\textsuperscript{-1}] \\ \hline 
1  & 0.0054        & 0.0654  \\\hline
2  & 0.001087      & 1.35    \\\hline
\end{tabular}
\caption{Delayed neutron data for the 2-D LRA BWR benchmark}\label{tab:B14delayedata}
\end{table}
The initiating perturbation is such that the absorption cross section in the region R (\autoref{fig:B14geom2d}) changes in this way:
\begin{equation*}
\frac{\Sigma a_2(t)}{\Sigma a_2(0)}=
\begin{cases}
1-0.0606184\cdot t & , t \leq 0.2 \text{s} \\
0.8787631 & , t > 0.2 \text{s} \\
\end{cases}
\qquad t=\text{time [s]}
\end{equation*}

The time step is 0.001 s and there are two discretizations with quads first order (\autoref{fig:B14grueso}, \autoref{fig:B14fino}).

The initial and final (t = 2 s without feedback effects) eigenvalue is shown in the \autoref{tab:B14kas}.

The average power density is shown in the \autoref{fig:B14denspow}.

The average temperature:

\begin{equation*}
\bar{T}=\frac{1} {V_{core}}\int_{Vcore} T(\mathbf{x}, t) \mathrm{d}V
\end{equation*}

is shown in the \autoref{fig:B14avgtemp}.

The maximum average power density and the time it happens is shown in the \autoref{tab:B14avgpower}.

\begin{table}
\centering
\begin{tabular}{|c|c|c|} \hline
\textbf{Case}  &  \textbf{Initial $k_{eff}$} & \textbf{Final $k_{eff}$} \\\hline
Coarse                &   0.99688                   &   1.01663                \\\hline
Fine                  &   0.99668                   &   1.01602                \\\hline
Reference \cite{refB14A1}&   0.99633                   &   1.01546                \\\hline
\end{tabular}
\caption{Static calculations at 0 and 2 s}\label{tab:B14kas}
\end{table}


\noindent
\begin{minipage}{7.5cm}
\captionsetup{type=figure}
\centering
\includegraphics[width=7cm]{B14grueso}
\captionof{figure}{Coarse mesh used to solve the 2-D LRA BWR benchmark}\label{fig:B14grueso}
\end{minipage}
\hfill
\begin{minipage}{7.5cm}
\captionsetup{type=figure}
\centering
\includegraphics[width=7cm]{B14fino}
\captionof{figure}{Fine mesh used to solve the 2-D LRA BWR benchmark}\label{fig:B14fino}
\end{minipage}

\begin{figure}[p]
\centering
\includegraphics[width=16cm]{B14avgdenspow}
\caption{Average power density of 2-D LRA BWR benchmark}\label{fig:B14denspow}
\end{figure}
\begin{figure}[p]
\centering
\includegraphics[width=16cm]{B14avgT}
\caption{Average temperature of 2-D LRA BWR benchmark}\label{fig:B14avgtemp}
\end{figure}
\begin{table}
\centering
\begin{tabular}{|c|c|c|} \hline
\textbf{Case}  &  $\bar{\mathbf{P}}_{max}$\textbf{[W/cm\textsuperscript{3}]} & \textbf{Time [s]} \\\hline
Coarse                &          5772               &     1.388                \\\hline
Fine                  &          5681               &     1.421                \\\hline
Reference \cite{refB14A1} &          5734               &     1.421                \\\hline
\end{tabular}
\caption{Maximum average power density}\label{tab:B14avgpower}
\end{table}
\section{Twigl benchmark}

These are two benchmarks which changes the speed of the perturbation.

There are two neutron groups and one group of delayed neutron precursors.

The \autoref{fig:geotwigl} shows the geometry used and the \autoref{tab:datatwigl} shows the materials data.

The delayed neutron fraction is $\beta=0.0075$ and the decay constant is $\lambda=0.08$ s\textsuperscript{-1}.

The energy per fission is proportional to the fission cross section.

The initial power is normalized to 1.

One of the cases is a step change in the thermal absorption cross section of the material 1; whereas the second case is
a ramp change in the same cross section \cite{twigl}.

The mesh is shown in the \autoref{fig:twigl4mesh}. Triangles second order elements are used.

The time step is 0.005 s and it is solved with the Crank Nicolson method.

The initial eigenvalue is $k_{eff}=0.91320$ and the final eigenvalue (after dividing the fission cross sections by the initial $k_{eff}$) is $k_{eff}=1.00385$.
So, the reactivity insertion from the critical state is 384 pcm.

\noindent
\begin{minipage}{9cm}
\captionsetup{type=figure}
\centering
\includegraphics[width=8cm]{twigl4}
\captionof{figure}{Geometry used to solve the Twigl benchmarks}\label{fig:geotwigl}
\end{minipage}
\hfill
\begin{minipage}{7cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
                    & \multicolumn{2}{|c|}{\textbf{Region}}     \\ \cline{2-3}
\textbf{Constant}                           & 1 and 2   & 3     \\ \hline 
D\textsuperscript{1} [cm]                   & 1.4       & 1.3   \\ \hline
D\textsuperscript{2} [cm]                   & 0.4       & 0.5   \\ \hline
$\Sigma_a^1$ [cm\textsuperscript{-1}]       & 0.01      & 0.008 \\ \hline
$\Sigma_a^2$ [cm\textsuperscript{-1}]       & 0.15      & 0.05  \\ \hline
$\nu\Sigma_f^1$ [cm\textsuperscript{-1}]    & 0.007     & 0.003 \\ \hline
$\nu\Sigma_f^2$ [cm\textsuperscript{-1}]    & 0.2       & 0.06  \\ \hline
$\Sigma^{1\to2}$ [cm\textsuperscript{-1}]   & 0.01      & 0.01  \\ \hline
$\chi^{1}$ [-]                              &  1        & 1     \\ \hline
$\chi^{2}$ [-]                              & 0         & 0     \\ \hline
v\textsuperscript{1} [cm/s]                 & 10\textsuperscript{7}     & 10\textsuperscript{7}  \\ \hline
v\textsuperscript{2} [cm/s]                 & 2 10\textsuperscript{5}   & 2 10\textsuperscript{5}\\ \hline
$\nu$\textsuperscript{1}                    & 2.43      &2.43     \\\hline                   
$\nu$\textsuperscript{2}                    & 2.43      &2.43     \\\hline                                                         
\end{tabular}
\captionof{table}{Twigl materials data}\label{tab:datatwigl}
\end{minipage}
\begin{figure}[h]
\centering
\includegraphics[width=8cm]{twigl4mesh}
\caption{Mesh used in the twigl benchmark}\label{fig:twigl4mesh}
\end{figure}
\subsection{Step perturbation}

The perturbation is $\Sigma_{a,1}^2(t)=\Sigma_{a,1}^2(0) \cdot 0.97666$. It means that the thermal absorption cross section of the material 1 is
reduced suddenly.

The \autoref{fig:twiglstepres} and the \autoref{tab:twiglstepres} show the power and the comparison with the reference solution \cite{twigl}.

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\centering
\includegraphics[width=8cm]{twiglstepres}
\captionof{figure}{Relative Power}\label{fig:twiglstepres}
\end{minipage}
\hfill
\begin{minipage}{7cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Time [s]}                           & \textbf{Milonga}   & \textbf{POLCA-T}     \\ \hline 
0  & 1         & 1     \\ \hline
0.1& 2.023     & 2.061 \\ \hline
0.2& 2.051     & 2.080 \\ \hline
0.3& 2.074     & 2.097 \\ \hline
0.4& 2.096     & 2.114 \\ \hline
0.5& 2.117     & 2.132 \\ \hline
\end{tabular}
\captionof{table}{Relative Power}\label{tab:twiglstepres}
\end{minipage}

\subsection{Ramp perturbation}
The perturbation is in the thermal absorption cross section of the material 1:

\begin{equation*}
\Sigma_{a,1}^2(t)=
\begin{cases}
\Sigma_{a,1}^2(0) \cdot \left(1-0.11667 \cdot t\right) & , t \leq 0.2 \text{s} \\
\Sigma_{a,1}^2(0) \cdot 0.97666 & , t > 0.2 \text{s} \\
\end{cases}
\end{equation*}

The \autoref{fig:twiglrampres} and the \autoref{tab:twiglrampres} show the power and the comparison with the reference solution \cite{twigl}.

\noindent
\begin{minipage}{8cm}
\captionsetup{type=figure}
\centering
\includegraphics[width=8cm]{twiglrampres}
\captionof{figure}{Relative Power}\label{fig:twiglrampres}
\end{minipage}
\hfill
\begin{minipage}{7cm}
\captionsetup{type=table}
\centering
\begin{tabular}{|c|c|c|}  \hline 
\textbf{Time [s]}                           & \textbf{Milonga}   & \textbf{POLCA-T}     \\ \hline 
0  & 1         & 1     \\ \hline
0.1& 1.311     & 1.308 \\ \hline
0.2& 1.952     & 1.961 \\ \hline
0.3& 2.045     & 2.076 \\ \hline
0.4& 2.069     & 2.093 \\ \hline
0.5& 2.092     & 2.111 \\ \hline
\end{tabular}
\captionof{table}{Relative Power}\label{tab:twiglrampres}
\end{minipage}
% no bibliography by default to avoid problems with non-installed biber
\printbibliography
