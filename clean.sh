rm -rf *~
rm -rf *.aux 
rm -rf *.log
rm -rf *.out
rm -rf *.toc
rm -rf *.bbl
rm -rf *.bcf
rm -rf *.blg
rm -rf *.run.xml 
for i in `cat .gitignore | grep -v baserev | grep -v gitex`; do rm -f $i; done
